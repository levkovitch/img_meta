local buf_meta = require("img_meta")

local image_name = arg[1]
if image_name == nil then error("set image file as first argument") end

local data = io.open(image_name, "r"):read("*a")

local mime, m_err = buf_meta.get_buffer_mime(data)
if mime == nil then error(m_err) end
local out, err = buf_meta.get_image_size(data, mime)
if out == nil then error(err) end

print("mime", mime)
print("width", out.width)
print("height", out.height)
