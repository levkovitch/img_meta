// img_meta.c

#include <assert.h>
#include <errno.h>
#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>
#include <magic.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define DEAD_BEAF(ptr) ptr = (void *)0xdeadbeaf


struct Size {
  int width;
  int height;
};


enum ImageType { None, Png, Jpg };

enum ImageType typeFromMime(const char *mime) {
  if (strcmp(mime, "image/png") == 0) {
    return Png;
  } else if (strcmp(mime, "image/jpg") || strcmp(mime, "image/jpeg")) {
    return Jpg;
  }

  return None;
}

int bigToLittle(int val) {
  uint8_t *bytes  = (uint8_t *)&val;
  int      retval = (int)(bytes[0] << 24) | (int)(bytes[1] << 16) |
               (int)(bytes[2] << 8 | (int)(bytes[3] << 0));
  return retval;
}

/**\param mime acceptable values: image/png, image/jpg or image/jpeg
 */
int getEncodedImageSize(const void *   data,
                        size_t         length,
                        enum ImageType type,
                        struct Size *  size) {
  assert(size != NULL);
  if (data == NULL || length == 0) {
    return EINVAL;
  }

  switch (type) {
  case Png: {
    // in png size stores as 2 big-endian integers starting at 16 bytes offset
    if (length < 16 + 8) {
      return EINVAL;
    }

    const int *i_ptr  = data;
    int        width  = i_ptr[4];
    int        height = i_ptr[5];

    size->width  = bigToLittle(width);
    size->height = bigToLittle(height);
    return 0;
  };
  case Jpg: {
    const uint8_t *buf = data;

    // For JPEGs, we need to read the first 12 bytes of each chunk.
    // We'll read those 12 bytes at buf+2...buf+14, i.e. overwriting the
    // existing buf.
    if (buf[0] == 0xFF && buf[1] == 0xD8 && buf[2] == 0xFF &&
        ((buf[3] == 0xE0 && buf[6] == 'J' && buf[7] == 'F' && buf[8] == 'I' &&
          buf[9] == 'F') /*JFIF*/
         || (buf[3] == 0xE1 && buf[6] == 'E' && buf[7] == 'x' &&
             buf[8] == 'i' && buf[9] == 'f') /*Exif*/)) {
      for (size_t i = 2; i < length;) {
        if (buf[i] == 0xFF) {
          if (buf[i + 1] == 0xC0 || buf[i + 1] == 0xC2) {
            int x = (buf[i + 7] << 8) + buf[i + 8];
            int y = (buf[i + 5] << 8) + buf[i + 6];

            size->width  = x;
            size->height = y;
            return 0;
          } else {
            int count = (buf[i + 2] << 8) + buf[i + 3] + /*tag size*/ 2;
            i += count;
          }
        } else {
          break;
        }
      }
    }

    return EINVAL;
  };
  default:
    return EINVAL;
  }
}

/**\warning you need free output string
 */
char *getEncodedImageMime(const void *data, size_t length) {
  if (data == NULL || length == 0) {
    return NULL;
  }

  magic_t magicCookie = magic_open(MAGIC_MIME_TYPE);
  if (magicCookie == NULL) {
    return NULL;
  }
  magic_load(magicCookie, NULL);


  const char *mime = magic_buffer(magicCookie, data, length);
  char *      out  = malloc(strlen(mime) + 1);
  strcpy(out, mime);
  magic_close(magicCookie);

  return out;
}

/**\brief get two strings from stack: image data and mime type - and put to
 * stack table with fields: width and height or nil and error string in case of
 * failure
 */
static int get_image_size(lua_State *state) {
  size_t      dataLength;
  const char *data = luaL_checklstring(state, 1, &dataLength);
  const char *mime = luaL_checkstring(state, 2);

  struct Size size;
  if (getEncodedImageSize(data, dataLength, typeFromMime(mime), &size) != 0) {
    lua_pushnil(state);
    lua_pushstring(state, "invalid image");
    return 2;
  }

  lua_createtable(state, 0, 2);
  lua_pushnumber(state, size.width);
  lua_setfield(state, -2, "width");
  lua_pushnumber(state, size.height);
  lua_setfield(state, -2, "height");

  return 1;
}

/**\brief get one string from stack: buffer - and put to stack mime type of
 * buffer or nil and error string
 */
static int get_buffer_mime(lua_State *state) {
  size_t      dataLength;
  const char *data = luaL_checklstring(state, 1, &dataLength);

  char *mime = getEncodedImageMime(data, dataLength);
  if (mime == NULL) {
    lua_pushnil(state);
    lua_pushstring(state, "invalid image");
    return 2;
  }

  lua_pushstring(state, mime);

  free(mime);
  DEAD_BEAF(mime);

  return 1;
}

int luaopen_img_meta(lua_State *state) {
  const luaL_Reg lua_BufMeta[] = {{"get_image_size", get_image_size},
                                  {"get_buffer_mime", get_buffer_mime},
                                  {NULL, NULL}};

  luaL_register(state, "buf_meta", lua_BufMeta);

  return 1;
}
